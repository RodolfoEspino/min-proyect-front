// creo una constante en la cual igualo a lo que importe la libreria express
const express = require('express');
const path = require('path');
const app = express();
const mongoose = require('mongoose');
const cors= require('cors')
const User =require('./models/userModel');
const Comment = require('./models/commentModel');
const Actores = require('./models/actoresModels');
const Tarea = require('./models/tareasModel');


//hacer que la app utilice json
app.use(express.json());


app.use(cors());

//conectar la app con mongo usando mongoose
mongoose.connect('mongodb+srv://admin:admin2021@cluster0.vjvbr.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
{ useNewUrlParser: true, useUnifiedTopology: true }, 
(error) => {
    if(error){
        console.log('Hubo un error al conectarse a la base de datos: ', error);
    }else{
        console.log('conectado a la base de datos');
    }
}
)


// raoutes (rutas)

app.get('/operacionesBasicas', (req, res) => {
    res.sendFile(path.join(__dirname+'/vistas/calculadora.html'))
})
app.get('/listaTareas', (req, res) => {
    res.sendFile(path.join(__dirname+'/vistas/toDoList.html'))
})

app.get('/direccion', (req, res) => {
    res.json({
        ciudad: 'Playa del carmen',
        calle: 'playa la costa',
        numeroDeCasa: 23,
        descpcionDeLaCasa:[
            'de dos pisos',
            'color chocolate',
            'reja negra'    
        ]
    })
})
app.get('/agradecimientos', (req, res) => {
    res.send(' agradesco a todos y todas por su colaboracion ')
});

app.get('/saludar', (req, res) => {
    res.send('Hola a todos desde mi aplicacion backend')

});

app.get('/user/RodolfoEspino', (req, res) => {
    res.json({
        name: 'Rodolfo Espino',
        edad: 37,
        hobbies:[
            ' descansar ',
            ' ver series '
        ]
    })
})

//rutas del CRUD para dar de alta una tarea  (create)
//ruta para dar de alta una tarea(create)

app.post('/tarea/user/:id', (req, res) =>{
    const userId = req.params.id
    const tareaRequestBody = req.body;
    //validar y obtener el usuario que el cliente nos manda
    User.findById(userId, (error, user) =>{
        if(error) {
            res.status(400).json({
                ok: false,
                msg: `hubo un problema al cargar el usuario`, 
            })
        } else {
            if(user){
                //guardamos la tarea en la db
                Tarea.create(tareaRequestBody, (error, tarea) => {
                    if(error){
                        res.status(500).json({
                            ok: false,
                            msg: 'hubo un problema al cargar una tarea'
                        })
                    }else{
                        //empujamos la tarea al arreglo de tareas del usuario ya validado
                        user.tareas.push(tarea)
                        //guardamos la modificavion que hicimos al usuario
                        user.save()
                        res.json({
                            ok: true,
                            msg: 'la tarea ha sido creada'
                        })
                    }
                })
            
            }
        }  
    })
})

//ruta para eliminar una tarea 
app.delete('/eliminar/tarea/:id', (req, res) =>{
    const tareaId = re.params.id;

    Tarea.findByIdAndDelete(tareId, (error)=> {
        if(error) {
            res.status(400).json({
                ok: false,
                msg: 'hubo un problema al eliminar tarea'
            })
        }else{
            res.json({
                ok: true,
                msg: 'tarea eliminada con exito'
            })
        }
    })
})

//ruta para actualizar tarea

app.put('/actualizar/tarea/:id', (req, res) => {
    const tareaId = req.params.id;
    const tareaEditada = req.body;

    Tarea.findByIdAndUpdate(tareaId, tareaEditada,{new: true}, (error, tarea) => {
        if(error) {
            res.status(400).json({
                ok: false,
                msg: 'hubo un problema al editar tarea'
            })
        }else{
            res.json({
                ok: true,
                tarea
            })
        }  
    })
})

//rutas del CRUD del usuario
//rutas para dar de alta un usuario
app.post('/user', (req, res) =>{
    const user= req.body
    console.log(user, );
    User.create(user, (error, user) =>{
        if(error){
            res.status(400).json({
                ok: false,
                msg: 'Hubo un problema al guardad el usuario'
            })
        }else{
            res.json({
                ok: true,
                msg: 'El usuario Ha sido creado',
                id: user._id,
            })
        }
    })    
})

//ruta para traer todos los usuarios (read)

app.get('/users/all', (req, res) => {
    User.find((error, users) => {
        if(error) {
            res.status(500).json({
                ok: false,
                msg:'hubo un problema al traer los usuarios',
                //eroro: error
                error
            })
        } else {
            res.json({
                ok: true,
                // users: users
                users
            })
        }    
    })
})

//ruta para trae un solo usuario por id(read)

app.get('/user/:id', (req, res) => {
    const idQueryParam = req.params.id
    User.findById(idQueryParam).populate('tareas').exec((error, user) => {
        if(error) {
            res.status(500).json({
                ok: false,
                msg: `Hubo un problema al traer el usuario con id ${idQueryParam}`,
                // error: error
                error
            })
        } else {
            res.json({
                ok: true,
                //user: user
                user
            })
        }
    })
})


//ruta para traer un solo susuario por nombre(read)

app.get('/user/byName/:nombre', (req, res) => {
    const nombreQueryParam = req.params.nombre
    User.find({nombre: nombreQueryParam}, (error, user) => {
        if(error){
            res.status(400).json({
                ok: false,
                msg: 'hubo un problema al traer el usuario con nombre ${nombreQueryParam}',
                //error: error
                error
            })
        } else {
            res.json({
                ok: true,
                user
            })
        }
    })
})

app.get('/user/bylastName/:apellido', (req, res) => {
    const apellidoQueryParam = req.params.apellido
    User.find({apellido: apellidoQueryParam}, (error, user) => {
        if(error){
            res.status(400).json({
                ok: false,
                msg: `hubo un problema al traer el usuario con apellido ${apellidoQueryParam}`,
                //error: error
                error
            })
        } else {
            res.json({
                ok: true,
                user
            })
        }
    })
})

//ruta para actualizar un usuario (update)

app.put('/user/actualizar/:id', (req, res) => {
    const idQueryParam = req.params.id;
    const userUpdated = req.body;
    User.findByIdAndUpdate(idQueryParam, userUpdated, {new: true}, (error, user) => {
        if(error) {
            res.status(400).json({
                ok: false,
                msg: `hubo un problema al actualizar el usuario con id ${idQueryParam}`,
                //error: error
                error
            })
        } else {
            res.json({
                ok: true,
                user
            })
        }
    })
})

// ruta para la eliminar un usuario
app.delete('/user/delete/:id', (req, res) =>{
    const userId = req.params.id
    User.findByIdAndDelete(userId, (error) =>{
        if(error) {
            res.status(400).json({
                ok: false,
                msg: `hubo un problema al borrar el usuario con id ${idQueryParam}`,
                //error: error
                error
            })
        } else {
            res.json({
                ok: true,
                msg: 'el usuario ha sido borrado con exito'
            })
        }   
    })
})


app.post('/comment', (req, res) =>{
     const comment= req.body
    console.log(comment);
    Comment.create(comment, (error) =>{
        if(error){
            res.status(400).json({
                ok: false,
                msg: 'Hubo un proble al guardar el comentario'
            })
        }else{
            res.json({
                ok: true,
                msg: 'El comentario ha sido guardado'
            })
        }
    })    
})

//ruta para almacenar datos de actores 
app.post('/actores', (req, res) =>{
    const actores= req.body
    console.log(actores);
    Actores.create(actores, (error) =>{
        if(error){
            res.status(400).json({
                ok: false,
                msg: 'Hubo un problema al guardad los datos del actor'
            })
        }else{
            res.json({
                ok: true,
                msg: 'los datos del actor han sido creados'
            })
        }
    })    
})
app.put('/actores/actualizar/:id', (req, res) => {
    const idQueryParam = req.params.id;
    const actoresUpdated = req.body;
    Actores.findByIdAndUpdate(idQueryParam, actoresUpdated, {new: true}, (error, user) => {
        if(error) {
            res.status(400).json({
                ok: false,
                msg: `hubo un problema al actualizar los datos del actor con id ${idQueryParam}`,
                //error: error
                error
            })
        } else {
            res.json({
                ok: true,
                user
            })
        }
    })
})
app.get('/actores/todos', (req, res) => {
    Actores.find((error, users) => {
        if(error) {
            res.status(500).json({
                ok: false,
                msg:'hubo un problema al traer los actores',
                //eroro: error
                error
            })
        } else {
            res.json({
                ok: true,
                // users: users
                users
            })
        }    
    })
})




// le digo a mi app en que puerto corra
// puertos disponibles 3000, 3001, 5000, 5001, 8080, 8081
app.listen(3000, (error) => {
    if (error) {
        console.log('hubo un error al iniciar en la app', error);
    } else {
        console.log('la app esta viva en el puerto 3000');
    }
    })
    