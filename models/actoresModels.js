const { urlencoded } = require('express');
const mongoose = require('mongoose');

const ActoresSchema = new mongoose.Schema({
nombre: String,
apellido: String,
edad: Number,
cumpleaños: String,
lugarDeNacimiento: String,
peliculas: String
});

module.exports = mongoose.model("Actores", ActoresSchema);