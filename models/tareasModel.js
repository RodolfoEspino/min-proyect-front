const mongoose = require('mongoose');

const TareaSchema = new mongoose.Schema({
    titulo: String,
    descripcion: String,
    creatAt: {type: Date, default: new Date},
    
})

module.exports = mongoose.model("Tarea", TareaSchema)